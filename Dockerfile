FROM registry.gitlab.com/epicdocker/bloonix_agent:1.1.0
LABEL image.name="epicsoft_bloonix_satellite" \
      image.description="Docker image for Bloonix satellite to work with Bloonix server" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2018 epicsoft.de / Alexander Schwarz" \
      license="MIT"

ENV BLOONIX_SATELLITE_VERSION="0.11-1" \
    BLOONIX_SATELLITE_CONFIGURATION_SKIP=false \
    EPICSOFT_LOG_LEVEL_DEBUG=false

#### https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/
RUN apt-get -y update \
 && apt-get -y install bloonix-satellite=${BLOONIX_SATELLITE_VERSION} \
 && apt-get -y autoclean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* \
 && rm -f /etc/bloonix/satellite/pki/*

#RUN mv /etc/epicsoft/entrypoint.sh /etc/epicsoft/entrypoint-agent.sh \
# && mv /etc/epicsoft/healtcheck.sh /etc/epicsoft/healtcheck-agent.sh

COPY [ "./etc", "/etc" ]

RUN chmod +x /etc/epicsoft/*.sh

#ENTRYPOINT [ "/etc/epicsoft/entrypoint.sh" ]

#HEALTHCHECK CMD [ "/etc/epicsoft/healtcheck.sh" ]
